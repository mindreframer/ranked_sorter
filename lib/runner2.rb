require "benchmark/ips"

module Runner2
  def self.run
    search_results_small = [
      "password",
      "The Word Award",
      "1password",
      "Word",
      "wordtracker",
      "Google AdWords",
    ]
    search_results_medium = search_results_small.dup

    6.times do
      # search_results_small.size * 2^6 - big array => 384 items
      search_results_medium.concat(search_results_medium)
    end

    search_results_big = search_results_small.dup

    8.times do
      # search_results_small.size * 2^8 - big array => 1536 items
      search_results_big.concat(search_results_big)
    end

    search_phrase = "word"
    sorters = [
      # Sorter::Algorithm0.new,
      Sorter::Algorithm1.new,
      Sorter::Algorithm2.new,
      Sorter::Algorithm21.new,
      Sorter::Algorithm22.new,
      Sorter::Algorithm23.new,
    ]

    bench_config = {:time => 2, :warmup => 0}

    puts "\n\nSMALL \n\n"
    Benchmark.ips do |x|
      x.config(bench_config)
      size = search_results_small.size
      sorters.each do |sorter|
        x.report("#{sorter.name} small (#{size})") do
          sorter.sort(search_phrase: search_phrase, search_results: search_results_small)
        end
      end
      x.compare!
    end

    puts "\n\nMEDIUM\n\n"
    Benchmark.ips do |x|
      x.config(bench_config)
      size = search_results_medium.size
      sorters.each do |sorter|
        x.report("#{sorter.name} medium (#{size})") do
          sorter.sort(search_phrase: search_phrase, search_results: search_results_medium)
        end
      end
      x.compare!
    end

    puts "\n\nBIG\n\n"
    Benchmark.ips do |x|
      x.config(bench_config)
      size = search_results_big.size
      sorters.each do |sorter|
        x.report("#{sorter.name} big (#{size})") do
          sorter.sort(search_phrase: search_phrase, search_results: search_results_big)
        end
      end
      x.compare!
    end
  end
end
