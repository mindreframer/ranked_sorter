module Sorter
  # establish a baseline by the Ruby std lib sorting algorithm
  # not correct, but helps with perspective
  class Algorithm0
    def name
      "Algorithm0: Array.sort"
    end
    def sort(search_phrase:, search_results:)
      search_results.sort
    end
  end
end
