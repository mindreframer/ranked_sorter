module Sorter
  class Algorithm1
    def name
      "Algorithm1: quick and dirty"
    end

    def sort(search_phrase: , search_results:)
      search_phrase = search_phrase.downcase
      final_result = []
      # 1. exact match
      tmp = search_results.select{|item| item.downcase == search_phrase}
      final_result.push(tmp)
      search_results = search_results - tmp

      # 2. Results where one of the words is an exact match
      tmp = search_results.select{|item| item.downcase.split(" ").include?(search_phrase) }
      final_result.push(tmp)
      search_results = search_results - tmp

      # 3. Results that start with the search string
      regex = Regexp.new("^#{search_phrase}", Regexp::IGNORECASE)
      tmp = search_results.select{|item| item.downcase =~ regex }
      final_result.push(tmp)
      search_results = search_results - tmp

      # 4. The rest, alphabetically sorted
      final_result.push(search_results.sort)
      final_result.flatten
    end
  end
end
