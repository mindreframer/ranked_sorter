module Sorter
  class Algorithm2
    def name
      "Algorithm2: readable, no regex"
    end
    def sort(search_phrase:, search_results:)
      phrase = search_phrase.downcase
      ranked = search_results.map do |item|
        [item, rank(item, phrase)]
      end

      ranked.sort_by { |item| item[1] }.map { |item| item[0] }
    end

    private

    # 1. First exact matches
    # 2. Results where one of the words is an exact match
    # 3. Results that start with the search string
    # 4. The rest, alphabetically sorted
    def rank(item, phrase)
      item = item.downcase
      case
      when item == phrase
        "a"
      when word_included?(item, phrase)
        "b"
      when starts_with?(item, phrase)
        "c"
      else
        "d-#{item}"
      end
    end

    def word_included?(item, phrase)
      item.split(" ").include?(phrase)
    end

    def starts_with?(item, phrase)
      item.start_with?(phrase)
    end
  end
end
