module Sorter
  class Algorithm23
    def name
      "Algorithm2.3: intermediate array gone"
    end
    def sort(search_phrase:, search_results:)
      phrase = search_phrase.downcase
      whitespaced_phrase = " #{phrase}"

      search_results.sort_by do |item|
        rank(item, phrase, whitespaced_phrase)
      end
    end

    private

    # 1. First exact matches
    # 2. Results where one of the words is an exact match
    # 3. Results that start with the search string
    # 4. The rest, alphabetically sorted
    def rank(item, phrase, whitespaced_phrase)
      item = item.downcase
      case
      when item == phrase
        "a"
      when word_included?(item, whitespaced_phrase)
        "b"
      when starts_with?(item, phrase)
        "c"
      else
        "d-#{item}"
      end
    end

    # This one is a bit tricky... We already ruled out the exact match in Req. 1
    # Words that start with the search phrase come after.
    # -> check for inclusion of the phrase with whitespace char prefix (tabs / newlines are not considered)
    #    is sufficient in exactly our case to fullfil requirement 2: one of the words is an exact match
    def word_included?(item, whitespaced_phrase)
      item.include?(whitespaced_phrase)
    end

    def starts_with?(item, phrase)
      item.start_with?(phrase)
    end
  end
end
