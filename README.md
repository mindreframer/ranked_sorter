# Sorter

Comparing different implementations for ranked auto-completion sorting algorithms


## Results
```
$ bin/benchmark
                                                             user     system      total        real       allocations      memsize
Algorithm0: Array.sort small (6)                         0.002743   0.000015   0.002758 (  0.002758)                2          128
Algorithm1: quick and dirty small (6)                    0.016719   0.001158   0.017877 (  0.017911)               51         3648
Algorithm2: readable, no regex small (6)                 0.007099   0.000145   0.007244 (  0.007291)               40         1792
Algorithm2.1: Array.split gone small (6)                 0.006873   0.000157   0.007030 (  0.007079)               27         1272
Algorithm2.2: prepared whitespaced_phrase small (6)      0.005161   0.000001   0.005162 (  0.005162)               23         1112
Algorithm2.3: intermediate array gone small (6)          0.003895   0.000096   0.003991 (  0.004003)               15          696
----------------------------------------------------------------------------------------------------------------------------------
Algorithm0: Array.sort medium (384)                      0.018642   0.000955   0.019597 (  0.019635)                2         3152
Algorithm1: quick and dirty medium (384)                 0.452305   0.001170   0.453475 (  0.453735)             2256       126544
Algorithm2: readable, no regex medium (384)              0.321566   0.000290   0.321856 (  0.322153)             2308       101536
Algorithm2.1: Array.split gone medium (384)              0.274373   0.000419   0.274792 (  0.275031)             1476        68256
Algorithm2.2: prepared whitespaced_phrase medium (384)   0.243203   0.000187   0.243390 (  0.243507)             1157        55496
Algorithm2.3: intermediate array gone medium (384)       0.174258   0.000924   0.175182 (  0.175978)              771        33912
----------------------------------------------------------------------------------------------------------------------------------
Algorithm0: Array.sort big (1536)                        0.064628   0.003315   0.067943 (  0.068012)                2        12368
Algorithm1: quick and dirty big (1536)                   1.699268   0.002884   1.702152 (  1.703312)             8976       507920
Algorithm2: readable, no regex big (1536)                1.337510   0.001567   1.339077 (  1.340101)             9220       405664
Algorithm2.1: Array.split gone big (1536)                1.110001   0.001054   1.111055 (  1.111813)             5892       272544
Algorithm2.2: prepared whitespaced_phrase big (1536)     0.946057   0.001647   0.947704 (  0.949236)             4613       221384
Algorithm2.3: intermediate array gone big (1536)         0.660599   0.000679   0.661278 (  0.661683)             3075       135288
```


```
# Without Ruby stdlib baseline implemention + different benchmarking library with comparisons
$ bin/benchmark2


SMALL

Warming up --------------------------------------
Algorithm1: quick and dirty small (6)
                         1.000  i/100ms
Algorithm2: readable, no regex small (6)
                         1.000  i/100ms
Algorithm2.1: Array.split gone small (6)
                         1.000  i/100ms
Algorithm2.2: prepared whitespaced_phrase small (6)
                         1.000  i/100ms
Algorithm2.3: intermediate array gone small (6)
                         1.000  i/100ms
Calculating -------------------------------------
Algorithm1: quick and dirty small (6)
                         77.633k (±26.2%) i/s -    132.915k in   1.960942s
Algorithm2: readable, no regex small (6)
                        162.226k (±25.6%) i/s -    273.258k in   1.926519s
Algorithm2.1: Array.split gone small (6)
                        180.976k (±23.2%) i/s -    302.825k in   1.918730s
Algorithm2.2: prepared whitespaced_phrase small (6)
                        193.394k (±24.2%) i/s -    321.027k in   1.913544s
Algorithm2.3: intermediate array gone small (6)
                        277.014k (±24.3%) i/s -    450.955k

Comparison:
Algorithm2.3: intermediate array gone small (6):   277014.1 i/s
Algorithm2.2: prepared whitespaced_phrase small (6):   193393.6 i/s - same-ish: difference falls within error
Algorithm2.1: Array.split gone small (6):   180975.5 i/s - same-ish: difference falls within error
Algorithm2: readable, no regex small (6):   162226.4 i/s - 1.71x  slower
Algorithm1: quick and dirty small (6):    77633.2 i/s - 3.57x  slower



MEDIUM

Warming up --------------------------------------
Algorithm1: quick and dirty medium (384)
                         1.000  i/100ms
Algorithm2: readable, no regex medium (384)
                         1.000  i/100ms
Algorithm2.1: Array.split gone medium (384)
                         1.000  i/100ms
Algorithm2.2: prepared whitespaced_phrase medium (384)
                         1.000  i/100ms
Algorithm2.3: intermediate array gone medium (384)
                         1.000  i/100ms
Calculating -------------------------------------
Algorithm1: quick and dirty medium (384)
                          2.293k (± 9.9%) i/s -      4.527k in   1.998333s
Algorithm2: readable, no regex medium (384)
                          3.221k (±12.2%) i/s -      6.310k in   1.997857s
Algorithm2.1: Array.split gone medium (384)
                          3.773k (±10.8%) i/s -      7.421k in   1.997814s
Algorithm2.2: prepared whitespaced_phrase medium (384)
                          4.243k (±12.4%) i/s -      8.280k in   1.997134s
Algorithm2.3: intermediate array gone medium (384)
                          5.970k (±10.5%) i/s -     11.728k in   1.996308s

Comparison:
Algorithm2.3: intermediate array gone medium (384):     5970.2 i/s
Algorithm2.2: prepared whitespaced_phrase medium (384):     4243.4 i/s - 1.41x  slower
Algorithm2.1: Array.split gone medium (384):     3772.8 i/s - 1.58x  slower
Algorithm2: readable, no regex medium (384):     3220.6 i/s - 1.85x  slower
Algorithm1: quick and dirty medium (384):     2293.1 i/s - 2.60x  slower



BIG

Warming up --------------------------------------
Algorithm1: quick and dirty big (1536)
                         1.000  i/100ms
Algorithm2: readable, no regex big (1536)
                         1.000  i/100ms
Algorithm2.1: Array.split gone big (1536)
                         1.000  i/100ms
Algorithm2.2: prepared whitespaced_phrase big (1536)
                         1.000  i/100ms
Algorithm2.3: intermediate array gone big (1536)
                         1.000  i/100ms
Calculating -------------------------------------
Algorithm1: quick and dirty big (1536)
                        591.978  (± 6.8%) i/s -      1.178k in   2.000099s
Algorithm2: readable, no regex big (1536)
                        756.194  (±11.4%) i/s -      1.491k in   1.999462s
Algorithm2.1: Array.split gone big (1536)
                        911.763  (±11.4%) i/s -      1.796k in   1.999079s
Algorithm2.2: prepared whitespaced_phrase big (1536)
                          1.044k (±12.3%) i/s -      2.052k in   1.999853s
Algorithm2.3: intermediate array gone big (1536)
                          1.520k (± 7.8%) i/s -      3.017k in   1.999273s

Comparison:
Algorithm2.3: intermediate array gone big (1536):     1519.6 i/s
Algorithm2.2: prepared whitespaced_phrase big (1536):     1044.4 i/s - 1.46x  slower
Algorithm2.1: Array.split gone big (1536):      911.8 i/s - 1.67x  slower
Algorithm2: readable, no regex big (1536):      756.2 i/s - 2.01x  slower
Algorithm1: quick and dirty big (1536):      592.0 i/s - 2.57x  slower
```
