RSpec.describe Sorter do
  let(:search_phrase) { 'word' }

  let(:search_results) { [
    'password',
    'The Word Award',
    '1password',
    'Word',
    'wordtracker',
    'Google AdWords'
  ] }

  let(:expected_results) { [
    'Word',
    'The Word Award',
    'wordtracker',
    '1password',
    'Google AdWords',
    'password'
  ] }

  it 'sorts the results according to the rules' do
    classes = [Sorter::Algorithm1, Sorter::Algorithm2, Sorter::Algorithm21, Sorter::Algorithm22]
    classes.each do |klazz|
      expect(
        klazz.new.sort(search_phrase: search_phrase, search_results: search_results)
      ).to eq expected_results
    end
  end
end
